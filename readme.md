﻿# Benefits Expert

## Business Need:
One of the critical functions that we provide for our clients is the ability to pay for their employees’ benefits packages. 
A portion of these costs are deducted from their paycheck, and we handle that deduction. 
Please demonstrate how you would code the following scenario:

  * The cost of benefits is $1000/year for each employee
  * Each dependent (children and possibly spouses) incurs a cost of $500/year
  * Anyone whose name starts with ‘A’ gets a 10% discount, employee or dependent

We’d like to see this calculation used in a web application where employers input employees and their dependents, and get a preview of the costs.
This is of course a contrived example. We want to know how you would implement the application structure and calculations and get a brief preview of how you work.
Please implement a web application based on these assumptions:

  * All employees are paid $2000 per paycheck before deductions
  * There are 26 paychecks in a year

##Tooling Requirements

* sqlexpress
* VS 2015
* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with npm)
* [nwb](https://github.com/insin/nwb/) - `npm install -g nwb`

## Building the application
1. clone the repo
2. run `.\fake ci` in powershell for first time install
3. open solution in visual studio
4. run the BenefitsExpert.FakeDataGenerator to populate db with fake data
5. open powershell to `cd BenefitsExpert\src\BenefitsExpert.Web\Assets\js\react-app`
6. run `nwb serve`
7. go back to visual studio and run `BenefitsExpert.Web`

## Server Stack
#### Database
  * Entity Framework 6
  * DB Migrations via RoundHouse
#### Web
  * ASP.NET MVC 5
  * Feature Folders
  * MediatR
  * Automapper
#### Dependency Injection
  * Structuremap
#### Testing
  * Fixie
  * Autofixture
#### Security
  * OAuth 2.0 via google+

## UI Stack
  * React views
  * Skeleton CSS
  * WebPack
  * ES 6 compiled to ES5 via Babel


#### Building UI

You do not need to run these locally; the build script will run it as part of the CI

* `nwb build` (production)
* `nwb build --set-env-NODE_ENV=development` (development)