﻿#r @"./tools/FAKE/tools/FakeLib.dll"
open System
open System.IO
open System.Net
open System.Text
open System.Xml
open Fake
open Fake.RoundhouseHelper
open Fake.FixieHelper
open Fake.AssemblyInfoFile
open Fake.EnvironmentHelper
open Fake.NpmHelper

(******************************************************************************************************************
This is the all encompassing build script for Benefits Expert.
It is used for;
- automated Continuous Integrations Builds,
- local developer enviroment setup,
- updating and rebuilding the database via RoundHouse


IMPORTANT NOTES:

ARGUEMENTS
--------------------------------------------------------------------------------------------------------------
Name                Default                                     Description
--------------------------------------------------------------------------------------------------------------
publish             False                                       If you want to publish the
                                                                nuget packages or not

packageId           15B2                                        This is used to append nuget
                                                                package version number.

******************************************************************************************************************)

let projectBaseName = "BenefitsExpert"
let buildConfig = "Release"
let setVersion = "0.0.0.1"
let version =
    match buildServer with
    | TeamCity -> buildVersion
    | _ -> setVersion

// Directories
let buildDir = Path.Combine(currentDirectory, "build")
let slnDir = combinePaths currentDirectory "src"
let packageDir = combinePaths slnDir "packages"

let resultDir = combinePaths buildDir "results"
let testDir = combinePaths buildDir "test"
let slnFiles = [
    combinePaths slnDir @"\BenefitsExpert.sln";
]

let sharedAssymFile = combinePaths slnDir "SharedAssemblyInfo.cs"
let environment = "LOCALDEV"
let uiAssetsDir =  combinePaths slnDir @"BenefitsExpert.Web\Assets\js\react-app"
let connectionStringPropertyName = "BenefitsExpert.DBContext"

let buildFiles (path : string) =
    path.EndsWith ".dll" ||  path.EndsWith ".config" || path.EndsWith "pdb" || path.EndsWith "sql"
     || path.EndsWith "xlsx" || path.EndsWith "csv"


//----------------------------------------------------------------------------------------
// Bootstap/ Nuget
RestorePackageDefaults.OutputPath = packageDir

Target "RestoreNuGet" (fun _ ->
    trace("Looping solution files => NugetRestore")
    for sln in slnFiles do
        sln |> RestoreMSSolutionPackages (fun p -> { p with OutputPath = packageDir})
 )
 //----------------------------------------------------------------------------------------


 //----------------------------------------------------------------------------------------
 // Build/ Clean
let execBuild buildParameters =
    for sln in slnFiles do
        trace("********** PROJECT: " + sln + " - CONFIG: " + buildConfig)
        build buildParameters sln
            |> DoNothing

Target "Clean" ( fun _ ->
    trace("Deleting Build Dirs: "+resultDir+";"+testDir+";"+buildDir)
    DeleteDirs [resultDir; testDir; buildDir ]
    CreateDir testDir
    CreateDir resultDir
)


Target "Compile" ( fun _ ->
    let properties =
        [
            "NoLogo", "True"
            "Configuration", buildConfig
            "BuildInParallel", "true"
        ]

    let buildParams defaults =
        { defaults with
            Verbosity = Some(Quiet)
            Properties = properties
            //Targets = [ "clean"; "compile" ]
        }

    execBuild buildParams
)


Target "GenerateAssemblyInfo" (fun _ ->
    CreateCSharpAssemblyInfo sharedAssymFile
        [
            Attribute.ComVisible false
            Attribute.Version version
            Attribute.FileVersion version
            Attribute.Company ("Sean Biefeld")
            Attribute.Copyright ("Copyright (c) Biefeld - "+DateTime.Now.Year.ToString())
            Attribute.Product ("Benefits Expert")
            Attribute.Configuration buildConfig
            Attribute.InformationalVersion version
        ]
)
//----------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------
// Database Migrations
let benefitsExpertDatabaseName = "BenefitsExpert.Database"
let benefitsExpertTestDatabaseName = "BenefitsExpert.Database.Tests"

let benefitsExpertDbConnEnvVar = "BenefitsExpert.DBContext"
let testDbConnEnvVar = "BenefitsExpert.Tests.DBContext"

let defaultBenefitsExpertDbConnString = String.Format("Server=.\sqlexpress;Database={0};Trusted_Connection=True;", benefitsExpertDatabaseName)
let defaultBenefitsExpertTestDbConnString = String.Format("Server=.\sqlexpress;Database={0};Trusted_Connection=True;", benefitsExpertTestDatabaseName)

type dbType =
| BenefitsExpertDb
| TestDb

let getEnvVarOrDefault name defaultVal =
    match environVarOrNone name with
    | Some envVar -> envVar
    | None -> defaultVal

let genConnString dbType altConnString =
    match dbType with
    | BenefitsExpertDb -> getEnvVarOrDefault benefitsExpertDbConnEnvVar altConnString
    | TestDb -> getEnvVarOrDefault testDbConnEnvVar altConnString

let beDatabaseConnectionString = genConnString BenefitsExpertDb defaultBenefitsExpertDbConnString
let beTestDatabaseConnectionString = genConnString TestDb defaultBenefitsExpertTestDbConnString

let dbScriptsDir = combinePaths slnDir @"DatabaseMigration\BenefitsExpert"

let roundhouseDir = combinePaths packageDir @"roundhouse.0.8.6\bin"
let roundhouseOutputDir = combinePaths roundhouseDir "output"
let roundhouseExePath = combinePaths roundhouseDir "rh.exe"

let rhParams database files drop environment connectionString defaults =
    { defaults with
        SqlFilesDirectory = files
        DatabaseName = database
        ConnectionString = connectionString
        Drop = drop
        CommandTimeout = 500
        OutputPath = roundhouseOutputDir
        Environment = environment
        Silent = true
    }

let RunRoundhouseUpdate database files environment connectionString =
    Roundhouse <| rhParams database files false environment connectionString

let DropUsingRoundhouse database environment connectionString =
    Roundhouse <| rhParams database "." true environment connectionString

Target "DropDatabases" ( fun _ ->
    DropUsingRoundhouse benefitsExpertDatabaseName "LOCALDEV" beDatabaseConnectionString
)

Target "DropTestDatabases" ( fun _ ->
    DropUsingRoundhouse benefitsExpertTestDatabaseName "TEST" beTestDatabaseConnectionString
)

Target "UpdateDatabases" ( fun _ ->
    RunRoundhouseUpdate benefitsExpertDatabaseName dbScriptsDir "LOCALDEV" beDatabaseConnectionString
)

Target "UpdateTestDatabases" ( fun _ ->
    RunRoundhouseUpdate benefitsExpertTestDatabaseName dbScriptsDir "TEST" beTestDatabaseConnectionString
)

// update all databases
Target "upd" DoNothing
// RebuildTestDatabases
Target "rtd" DoNothing
// RebuildAllDatabases
Target "rdb" DoNothing
//----------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------
// Unit Tests
let fixieConsolePath = findToolInSubPath "Fixie.Console.exe" (packageDir @@ "Fixie")

Target "CopyFilesForTesting" (fun _ ->
    trace("Executing tests.")
    let ext (ext:string) = slnDir + @"\*Test\bin\" + buildConfig + @"\*" + ext
    let filesForTest = !! (ext "ext") ++ (ext "dll") ++ (ext "config") ++ (ext "pdb") ++ (ext "sql") ++ (ext "xlsx") ++ (ext "csv")
    CopyTo testDir filesForTest
)

Target "UpdateTestDbConnectionString" (fun _ ->
    let configFile = combinePaths testDir "BenefitsExpert.Tests.dll.config"
    trace("*** Test Connection String: " + beTestDatabaseConnectionString)
    updateConnectionString connectionStringPropertyName beTestDatabaseConnectionString configFile
)

Target "RunUnitTests" (fun _ ->

    let testFiles = !! (testDir @@ "*Test.dll")
    for file in testFiles do trace("**********TESTFILE********* "+file)

    testFiles
    |> Fixie (fun p -> { p with ToolPath = fixieConsolePath; WorkingDir = testDir; TimeOut = TimeSpan.FromMinutes 10.; CustomOptions = ["NUnitXml", resultDir + @"\TestResults.xml" :> Object; "TeamCity", "off" :> Object] })
)

Target "rut" DoNothing
Target "rat" DoNothing
//----------------------------------------------------------------------------------------


// UI
Target "ui" (fun _ -> 

    printfn "Install npm packages"

    let npmResult = ExecProcessAndReturnMessages (fun info ->
        info.FileName <- "where"
        info.Arguments <- "npm.cmd" ) (TimeSpan.FromMinutes 5.0)

    let npmLocation = npmResult.Messages.Item(0)

    Npm (fun p ->
              { p with
                  Command = Install Standard
                  NpmFilePath = npmLocation
                  WorkingDirectory = uiAssetsDir
              })

    printfn "Run nwb build"

    Npm (fun p ->
          { p with
              Command = Run("build")
              NpmFilePath = npmLocation
              WorkingDirectory = uiAssetsDir
          })
)

//----------------------------------------------------------------------------------------
// Help

Target "?" DoNothing
Target "-h" DoNothing

let writeHelpHeader =
    trace("")
    trace("********************************  HELP ********************************")
    trace("")
    trace("This build script has the following common build task aliases set up:")

let writeHelpFooter =
    trace("")
    trace(" For a complete list of build tasks, view build.fsx")
    trace("")
    trace("***********************************************************************")

let writeHelpSectionHeader description =
    trace("")
    trace(sprintf " %s" description)

let writeHelpForAlias alias description =
    trace(sprintf "  > %s = %s" alias description)

Target "help" (fun _ ->
    writeHelpHeader
    writeHelpForAlias "(default)" "Intended for first build or when you want a fresh, clean local copy"
    writeHelpForAlias "dev" "Optimized for local dev; Most noteably UPDATES databases instead of REBUILDING"
    writeHelpForAlias "ci" "Continuous Integration build (long and thorough) with packaging"
    writeHelpSectionHeader "Database Maintence"
    writeHelpForAlias "udb" "Update the Database to the latest version (leave db up to date with migration scripts)"
    writeHelpForAlias "rdb" "Rebuild all Databases (dev and test) to the latest version from scratch (useful while working on the schema)"
    writeHelpSectionHeader "Running Tests"
    writeHelpForAlias "rat" "Run all tests"
    writeHelpForAlias "rut" "Run unit tests"
    writeHelpSectionHeader "Development"
    writeHelpForAlias "ui" "Run UI build"
    writeHelpFooter
)

//----------------------------------------------------------------------------------------


Target "Default" (fun _ ->
    trace ("Building Version: "+version)
)


Target "ci" (fun _ ->
    trace ("Running Continuous Integration Build!")
)

//Compilation and bootstrapping
"RestoreNuGet" ==> "Clean"
"Clean" ==> "Compile"
"GenerateAssemblyInfo" ==> "Compile"

//Testing
"Compile" ==> "CopyFilesForTesting"
"CopyFilesForTesting" ==> "RunUnitTests"
"UpdateTestDbConnectionString" ==> "RunUnitTests"
"rtd" ==> "RunUnitTests"

"RunUnitTests" ==> "rat"

"RunUnitTests" ==> "rut"

//Roundhouse
"DropTestDatabases" ==> "rtd"
"UpdateTestDatabases" ==> "rtd"

"UpdateDatabases" ==> "upd"
"UpdateTestDatabases" ==> "upd"

"rtd" ==> "rdb"
"DropDatabases" ==> "rdb"
"UpdateDatabases" ==> "rdb"

// helps
"help" ==> "?"
"help" ==> "-h"

"Compile" ==> "ci"
"ui" ==> "ci"

"Compile" ==> "Default"

RunTargetOrDefault "Default"
