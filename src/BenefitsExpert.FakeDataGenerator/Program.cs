﻿namespace BenefitsExpert.FakeDataGenerator
{
    using System;
    using System.Collections.Generic;
    using Core;
    using Core.Domain;

    public class Program
    {
        public static void Main(string[] args)
        {
            var dbContext = new BenefitsExpertContext();

            for (int i = 0; i < 1000; i++)
            {
                var random = new Random(DateTime.Now.Millisecond);
                var firstNameRandom = random.Next(0, 999);
                var firstName = RandomDataStore.GivenNames[firstNameRandom].UppercaseFirst();

                var lastNameRandom = random.Next(0, 999);
                var lastName = RandomDataStore.SurNames[lastNameRandom].UppercaseFirst();

                var departmentRandom = random.Next(0, 6);
                var department = RandomDataStore.Department[departmentRandom];

                var locationRandom = random.Next(0, 13);
                var location = RandomDataStore.Location[locationRandom];

                var romanNumerals = new Dictionary<int, string>
                {
                    {1, "I"},
                    {2, "II"},
                    {3, "III"},
                    {4, "IV"},
                    {5, "V"},
                };
                var jobTitleRandom = romanNumerals[random.Next(1, 5)];
                var jobTitle = $"Specialist {jobTitleRandom}";

                var user = new User
                {
                    FirstName = firstName,
                    LastName = lastName,
                    Email = $"{firstName}.{lastName}.{Guid.NewGuid().ToString().Substring(0, 7)}@test.com",
                    Department = department,
                    Location = location,
                    EmployeeNumber = Guid.NewGuid().ToString().Substring(0,6),
                    IsAdmin = false,
                    JobTitle = jobTitle,
                };

                user.Dependents = GenerateDependents(user, random);

                dbContext.Users.Add(user);

                dbContext.SaveChanges();

                Console.WriteLine($"User created: {user.LastName}, {user.FirstName}");
            }

            Console.WriteLine("Done generating employees");
            Console.ReadLine();
        }

        private static List<Dependent> GenerateDependents(User user, Random random)
        {
            var generatedDependents = new List<Dependent>();
            var numberOfDependents = random.Next(0,6);

            for (int i = 0; i < numberOfDependents; i++)
            {
                var firstNameRandom = random.Next(0, 999);
                var firstName = RandomDataStore.GivenNames[firstNameRandom].UppercaseFirst();
                var relationshipRandom = i == 0 ? 1 : 0;

                generatedDependents.Add(new Dependent
                {
                    Employee = user,
                    FirstName = firstName,
                    LastName = user.LastName,
                    Relationship = relationshipRandom
                });
            }

            return generatedDependents;
        }
    }
}
