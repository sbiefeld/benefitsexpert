﻿namespace BenefitsExpert.Test
{
    using System;
    using DependencyResolution;
    using StructureMap;

    public static class TestDependencyScope
    {
        private static IContainer _currentNestedContainer;

        public static void Begin()
        {
            if (_currentNestedContainer != null)
                throw new Exception("Cannot begin test dependency scope. Another dependency scope is still in effect.");

            _currentNestedContainer = IoC.Container.GetNestedContainer();
        }

        public static IContainer CurrentNestedContainer
        {
            get
            {
                if (_currentNestedContainer == null)
                    throw new Exception($"Cannot access the {nameof(CurrentNestedContainer)}. There is no dependency scope in effect.");

                return _currentNestedContainer;
            }
        }

        public static void End()
        {
            if (_currentNestedContainer == null)
                throw new Exception("Cannot end test dependency scope. There is no dependency scope in effect.");

            _currentNestedContainer.Dispose();
            _currentNestedContainer = null;
        }
    }
}