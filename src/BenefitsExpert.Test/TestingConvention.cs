﻿namespace BenefitsExpert.Test
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Validation;
    using System.Linq;
    using System.Reflection;
    using BenefitsExpert.Core.Domain;
    using BenefitsExpert.Infrastructure.Mapping;
    using AutoFixture.Builders;
    using BenefitsExpert.Core;
    using Fixie;
    using Ploeh.AutoFixture.Kernel;
    using Respawn;
    using Fixture = Ploeh.AutoFixture.Fixture;

    public class TestingConvention : Convention
    {
        public TestingConvention()
        {
            Classes
                .NameEndsWith("Tests");

            Methods
                .Where(method => method.IsVoid() || method.IsAsync());

            ClassExecution
                .Wrap<InitializeAutoMapper>();

            CaseExecution
                .Wrap<ExplainEntityValidationErrors>()
                .Wrap<ResetDatabase>()
                .Wrap<NestedContainerPerCase>();

            Parameters
                .Add<AutoFixtureParameterSource>();
        }
    }

    public class InitializeAutoMapper : ClassBehavior
    {
        public void Execute(Class context, Action next)
        {
            new AutoMapperRegistry();
            next();
        }
    }

    public class ExplainEntityValidationErrors : CaseBehavior
    {
        public void Execute(Case context, Action next)
        {
            next();

            foreach (var exception in context.Exceptions.OfType<DbEntityValidationException>())
            {
                foreach (var entityError in exception.EntityValidationErrors)
                {
                    var entity = entityError.Entry.Entity.GetType().Name;

                    foreach (var validationError in entityError.ValidationErrors)
                    {
                        var property = validationError.PropertyName;
                        var error = validationError.ErrorMessage;
                        Console.WriteLine($"\t\t\t{entity}.{property}: {error}");
                    }
                }
            }
        }
    }

    public class ResetDatabase : CaseBehavior
    {
        public void Execute(Case context, Action next)
        {
            var checkpoint = new Checkpoint
            {
                SchemasToExclude = new[] { "RoundhousE" },
                TablesToIgnore = new[] { "sysdiagrams" }
            };

            checkpoint.Reset(BenefitsExpertContext.ConnectionString);

            next();
        }
    }

    public class NestedContainerPerCase : CaseBehavior
    {
        public void Execute(Case context, Action next)
        {
            TestDependencyScope.Begin();
            next();
            TestDependencyScope.End();
        }
    }

    public class AutoFixtureParameterSource : ParameterSource
    {
        public IEnumerable<object[]> GetParameters(MethodInfo method)
        {
            // Produces a randomly-populated object for each
            // parameter declared on the test method, using
            // a Fixture that has our customizations.

            var fixture = new Fixture();

            CustomizeAutoFixture(fixture);

            var specimenContext = new SpecimenContext(fixture);

            var allEntitiesAttribute =
               method.GetCustomAttributes<AllEntities>(true).SingleOrDefault();

            if (allEntitiesAttribute != null)
            {
                return typeof(Entity<>).Assembly.GetTypes()
                    .Where(t => t.IsSubclassOf(typeof(Entity<Guid>)))
                    .Where(t => !t.IsAbstract)
                    .Except(allEntitiesAttribute.Except)
                    .Select(entityType => new[] { specimenContext.Resolve(entityType) })
                    .ToArray();
            }

            var parameterTypes = method.GetParameters().Select(x => x.ParameterType);

            var arguments = parameterTypes.Select(specimenContext.Resolve).ToArray();

            return new[] { arguments };
        }

        private static void CustomizeAutoFixture(Fixture fixture)
        {
            var propertyBuilders = typeof(BoolAlwaysTrueBuilder)
                .Assembly
                .GetTypes()
                .Where(t => !t.IsAbstract && typeof(ISpecimenBuilder).IsAssignableFrom(t))
                .Select(Activator.CreateInstance)
                .Cast<ISpecimenBuilder>();

            foreach (var propertyBuilder in propertyBuilders)
                fixture.Customizations.Add(propertyBuilder);
        }
    }

    [AttributeUsage(AttributeTargets.Method)]
    class AllEntities : Attribute
    {
        public AllEntities()
        {
            Except = new Type[] { };
        }

        public Type[] Except { get; set; }
    }
}
