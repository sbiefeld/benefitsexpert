﻿namespace BenefitsExpert.Test.Core.Domain
{
    using BenefitsExpert.Core.Domain;
    using Should;
    using static Testing;

    public class DeductionTests
    {
        public void ShouldCalculateEmployeeCostNoDependentsNoDiscountCorrectly(User employee, Deduction deduction)
        {
            employee.FirstName = "Phillip";
            employee.LastName = "Fry";

            Save(employee, deduction);

            deduction.CalculateAllDependentsCostPerYear(employee.Dependents).ShouldEqual(0m);
            deduction.CalculateTotalCostPerYear(employee, employee.Dependents).ShouldEqual(1000m);
        }

        public void ShouldCalculateEmployeeCostNoDependentsWithDiscountForFirstName(User employee, Deduction deduction)
        {
            employee.FirstName = "Arnold";
            employee.LastName = "Fry";

            Save(employee, deduction);
            
            deduction.CalculateTotalCostPerYear(employee, employee.Dependents).ShouldEqual(900m);
        }

        public void ShouldCalculateEmployeeCostWithDependentsWithNoDiscount(User employee, Dependent dependent, Deduction deduction)
        {
            employee.FirstName = "Firnold";
            employee.LastName = "Fry";

            dependent.FirstName = "Turanga";
            dependent.LastName = "Leela";

            employee.Dependents.Add(dependent);
            dependent.Employee = employee;

            Save(employee, dependent, deduction);

            deduction.CalculateAllDependentsCostPerYear(employee.Dependents).ShouldEqual(500m);
            deduction.CalculateTotalCostPerYear(employee, employee.Dependents).ShouldEqual(1500m);
        }

        public void ShouldCalculateDiscountEmployeeCostAndTwoDiscountedDependentDeductions(User employee, Dependent dependent1, Dependent dependent2, Dependent dependent3, Deduction deduction)
        {
            employee.FirstName = "Arnold";
            employee.LastName = "Fry";

            dependent1.FirstName = "Turanga";
            dependent1.LastName = "Leela";

            dependent2.FirstName = "Abigail";
            dependent2.LastName = "Rockdale";

            dependent3.FirstName = "Adam";
            dependent3.LastName = "Anise";

            employee.Dependents.Add(dependent1);
            dependent1.Employee = employee;

            employee.Dependents.Add(dependent2);
            dependent2.Employee = employee;

            employee.Dependents.Add(dependent3);
            dependent3.Employee = employee;

            Save(employee, dependent1, dependent2, dependent3, deduction);

            deduction.CalculateAllDependentsCostPerYear(employee.Dependents).ShouldEqual(1400m);
            deduction.CalculateTotalCostPerYear(employee, employee.Dependents).ShouldEqual(2300m);
        }
    }
}
