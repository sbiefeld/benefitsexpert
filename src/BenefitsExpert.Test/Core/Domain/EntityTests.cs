﻿namespace BenefitsExpert.Test.Core.Domain
{
    using System;
    using System.Linq;
    using BenefitsExpert.Core.Domain;

    public class EntityTests
    {
        [AllEntities]
        public void ShouldPersist<TEntity>(TEntity entity) where TEntity : Entity<Guid>
        {
            Testing.Save(entity);

            Testing.Transaction(db =>
            {
                var loaded = Queryable.Single<TEntity>(db.Set<TEntity>());

                loaded.Id.ShouldMatch(entity.Id);
            });
        }
    }
}