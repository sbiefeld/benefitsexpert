﻿namespace BenefitsExpert.Test.AutoFixture.Builders
{
    using System.Reflection;
    using Ploeh.AutoFixture.Kernel;

    public class ByteArrayNotEmpty : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            var property = request as PropertyInfo;

            if (property != null && (property.PropertyType == typeof(byte[])))
            {
                return new[] {(byte) 1};
            }

            return new NoSpecimen(request);
        }
    }
}