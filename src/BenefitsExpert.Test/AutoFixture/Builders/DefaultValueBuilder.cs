﻿namespace BenefitsExpert.Test.AutoFixture.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using BenefitsExpert.Core.Domain;
    using BenefitsExpert.Core.Extensions;
    using Ploeh.AutoFixture;
    using Ploeh.AutoFixture.Kernel;

    public class DefaultValueBuilder : ISpecimenBuilder
    {
        private readonly Dictionary<PropertyInfo, object> defaultValues = new Dictionary<PropertyInfo, object>();

        private readonly Dictionary<PropertyInfo, Func<object, ISpecimenContext, object>> defaultBuilders = new Dictionary<PropertyInfo, Func<object, ISpecimenContext, object>>();

        private readonly Dictionary<Tuple<Type, string>, Func<object, ISpecimenContext, object>> defaultBaseTypeBuilders = new Dictionary<Tuple<Type, string>, Func<object, ISpecimenContext, object>>();

        private readonly Dictionary<Type, Func<ISpecimenContext, object, object>> factories = new Dictionary<Type, Func<ISpecimenContext, object, object>>();

        private readonly Dictionary<PropertyInfo, Func<ISpecimenContext, object, object>> propertyFactories = new Dictionary<PropertyInfo, Func<ISpecimenContext, object, object>>();
        
        public DefaultValueBuilder()
        {
            AddUserDefaults();
            AddDeductionDefaults();
        }

        private void AddDeductionDefaults()
        {
            For<Deduction>().SetDefaultValue(d => d.NameStartsWithDiscountRule, "A");
            For<Deduction>().SetDefaultValue(d => d.PaychecksPerYear, 26);
            For<Deduction>().SetDefaultValue(d => d.DeductionPerEmployeePerYear, 1000m);
            For<Deduction>().SetDefaultValue(d => d.DependentDeductionPerYear, 500m);
            For<Deduction>().SetDefaultValue(d => d.DiscountPercentage, 0.1m);
            For<Deduction>().SetDefaultValue(d => d.DefaultPayPerPaycheck, 2000m);
            For<Deduction>().SetDefaultValue(d => d.Year, DateTime.UtcNow.Year);
        }

        private void AddUserDefaults()
        {
            For<User>().ConstrainStringSize(u => u.FirstName, 50);
            For<User>().ConstrainStringSize(u => u.LastName, 50);
        }

        public object Create(object request, ISpecimenContext context)
        {
            var type = request as Type;

            if (type != null && factories.ContainsKey(type))
            {
                return factories[type].Invoke(context, request);
            }

            var property = request as PropertyInfo;

            if (property == null)
            {
                return new NoSpecimen(request);
            }

            if (defaultValues.ContainsKey(property))
            {
                var value = defaultValues[property];
                return value;
            }

            if (defaultBuilders.ContainsKey(property))
            {
                var builder = defaultBuilders[property];
                var value = builder(request, context);
                return value;
            }

            if (propertyFactories.ContainsKey(property))
            {
                var factory = propertyFactories[property];
                return factory(context, request);
            }

            var defaultBaseTypeBuilder = defaultBaseTypeBuilders.FirstOrDefault(b => b.Key.Item2 == property.Name && property.DeclaringType.IsAssignableTo(b.Key.Item1));

            if (defaultBaseTypeBuilder.Key != null)
            {
                var builder = defaultBaseTypeBuilder.Value;
                var value = builder(request, context);
                return value;
            }

            return new NoSpecimen(request);
        }

        public void SetDefaultValue(PropertyInfo property, object value)
        {
            defaultValues[property] = value;
        }

        public void SetDefaultBuilder(PropertyInfo property, Func<object, ISpecimenContext, object> builder)
        {
            defaultBuilders[property] = builder;
        }

        public void SetPropertyFactory(PropertyInfo property, Func<ISpecimenContext, object, object> factory)
        {
            propertyFactories[property] = factory;
        }

        public void SetDefaultBaseTypeBuilder(Type baseType, PropertyInfo property, Func<object, ISpecimenContext, object> builder)
        {
            defaultBaseTypeBuilders[new Tuple<Type, string>(baseType, property.Name)] = builder;
        }

        public void SetFactory<T>(Func<ISpecimenContext, object, object> factory)
        {
            factories[typeof(T)] = factory;
        }

        private TypeWrapper<T> For<T>()
        {
            return new TypeWrapper<T>(this);
        }

        private BaseTypeWrapper<T> ForAllDerivedTypesOf<T>()
        {
            return new BaseTypeWrapper<T>(this);
        }

        private class BaseTypeWrapper<T>
        {
            private readonly DefaultValueBuilder builder;

            public BaseTypeWrapper(DefaultValueBuilder builder)
            {
                this.builder = builder;
            }

            public BaseTypeWrapper<T> ConstrainStringSize(Expression<Func<T, object>> expr, int maximumLength)
            {
                var property = expr.AsPropertyInfo();

                builder.SetDefaultBaseTypeBuilder(
                    typeof(T),
                    property,
                    (p, context) =>
                    {
                        var stringGenerator = new ConstrainedStringGenerator();
                        var constrainedStringRequest = new ConstrainedStringRequest(maximumLength);
                        return stringGenerator.Create(constrainedStringRequest, context);
                    });

                return this;
            }
        }

        private class TypeWrapper<T>
        {
            private readonly DefaultValueBuilder builder;

            public TypeWrapper(DefaultValueBuilder builder)
            {
                this.builder = builder;
            }

            public TypeWrapper<T> SetDefaultValue(Expression<Func<T, object>> expression, object value)
            {
                var propertyInfo = expression.AsPropertyInfo();
                builder.SetDefaultValue(propertyInfo, value);
                return this;
            }

            public TypeWrapper<T> ConstrainStringSize(Expression<Func<T, object>> expr, int maximumLength)
            {
                var property = expr.AsPropertyInfo();

                builder.SetDefaultBuilder(
                    property,
                    (p, context) =>
                    {
                        var stringGenerator = new ConstrainedStringGenerator();
                        var constrainedStringRequest = new ConstrainedStringRequest(maximumLength);
                        return stringGenerator.Create(constrainedStringRequest, context);
                    });

                return this;
            }

            public TypeWrapper<T> ResolveUsing(Expression<Func<T, object>> expr, Func<ISpecimenContext, object, object> factory)
            {
                var property = expr.AsPropertyInfo();

                builder.SetPropertyFactory(property, factory);

                return this;
            }

            public TypeWrapper<T> BuildUsing(Func<ISpecimenContext, object, object> factory)
            {
                builder.SetFactory<T>(factory);
                return this;
            }
        }
    }
}
