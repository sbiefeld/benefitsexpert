namespace BenefitsExpert.Test.AutoFixture
{
    using System.Linq;
    using DependencyResolution;
    using Builders;
    using Ploeh.AutoFixture;
    using Ploeh.AutoFixture.Kernel;

    public abstract class AutoFixtureCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customizations.Add(new IgnoreVirtualMembers());
            fixture.Customizations.Add(new DefaultValueBuilder());
            fixture.Customizations.Add(new ByteArrayNotEmpty());
            fixture.Customizations.Add(new BoolAlwaysTrueBuilder());
            fixture.Customizations.Add(new IdOmitterBuilder());
            fixture.Customizations.Add(new OmitListBuilder());

            AddTypeSpecificCustomizations(fixture);

            RemoveBehavior<ThrowingRecursionBehavior>(fixture);
            fixture.Behaviors.Add(new OmitOnRecursionBehavior(2));
        }

        protected abstract void CustomizeFixture(IFixture fixture, StructureMapDependencyScope scope);

        private static void RemoveBehavior<T>(IFixture fixture)
            where T : class, ISpecimenBuilderTransformation
        {

            var behaviorsToDelete = fixture.Behaviors.Where(x => (x as T) != null).ToList();

            foreach (var behavior in behaviorsToDelete)
            {
                fixture.Behaviors.Remove(behavior);
            }
        }

        private void AddTypeSpecificCustomizations(IFixture fixture)
        {
            
        }
    }
}