﻿namespace BenefitsExpert.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using MediatR;
    using Newtonsoft.Json;
    using Should;

    public static class Assertions
    {
        public static void ShouldMatch<T>(this IEnumerable<T> actual, params T[] expected)
            => expected.ShouldMatch(actual.ToArray());

        public static void ShouldMatch<T>(this T actual, T expected)
            => Json(actual).ShouldEqual(Json(expected), "Expected two objects to match on all properties.");

        private static string Json<T>(T actual)
            => JsonConvert.SerializeObject(actual, Formatting.Indented);

        public static void ShouldValidate<TResponse>(this IRequest<TResponse> message)
        {
            var validator = Testing.Validator(message);

            validator.ShouldNotBeNull($"There is no validator for {message.GetType()} messages");

            var result = validator.Validate(message);

            var indentedErrorMessages = result
                .Errors
                .OrderBy(x => x.ErrorMessage)
                .Select(x => "    " + x.ErrorMessage)
                .ToArray();

            var actual = String.Join(Environment.NewLine, indentedErrorMessages);

            result.IsValid.ShouldBeTrue(
                $"Expected no validation errors, but found {result.Errors.Count}:{Environment.NewLine}{actual}");
        }

        public static void ShouldValidate<TResponse>(this IAsyncRequest<TResponse> message)
        {
            var validator = Testing.Validator(message);

            validator.ShouldNotBeNull($"There is no validator for {message.GetType()} messages");

            var result = validator.Validate(message);

            var indentedErrorMessages = result
                .Errors
                .OrderBy(x => x.ErrorMessage)
                .Select(x => "    " + x.ErrorMessage)
                .ToArray();

            var actual = String.Join(Environment.NewLine, indentedErrorMessages);

            result.IsValid.ShouldBeTrue(
                $"Expected no validation errors, but found {result.Errors.Count}:{Environment.NewLine}{actual}");
        }

        public static void ShouldNotValidate<TResponse>(this IRequest<TResponse> message, params string[] expectedErrors)
        {
            var validator = Testing.Validator(message);

            validator.ShouldNotBeNull($"There is no validator for {message.GetType()} messages");

            var result = validator.Validate(message);

            result.IsValid.ShouldBeFalse("Expected validation errors, but the message passed validation.");

            var actual = result.Errors
                .OrderBy(x => x.ErrorMessage)
                .Select(x => x.ErrorMessage)
                .ToArray();

            actual.ShouldEqual(expectedErrors.OrderBy(x => x).ToArray());
        }

        public static void ShouldNotValidate<TResponse>(this IAsyncRequest<TResponse> message, params string[] expectedErrors)
        {
            var validator = Testing.Validator(message);

            validator.ShouldNotBeNull($"There is no validator for {message.GetType()} messages");

            var result = validator.Validate(message);

            result.IsValid.ShouldBeFalse("Expected validation errors, but the message passed validation.");

            var actual = result.Errors
                .OrderBy(x => x.ErrorMessage)
                .Select(x => x.ErrorMessage)
                .ToArray();

            actual.ShouldEqual(expectedErrors.OrderBy(x => x).ToArray());
        }
    }
}