﻿namespace BenefitsExpert.Test
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using BenefitsExpert.Core;
    using BenefitsExpert.Core.Domain;
    using BenefitsExpert.Infrastructure;
    using FluentValidation;
    using MediatR;
    using StructureMap;

    public static class Testing
    {
        private static IContainer Container => TestDependencyScope.CurrentNestedContainer;

        public static T Resolve<T>()
        {
            return Container.GetInstance<T>();
        }

        public static object Resolve(Type type)
        {
            return Container.GetInstance(type);
        }

        public static void Inject<T>(T instance) where T : class
        {
            Container.Inject(instance);
        }

        public static void LogSql()
        {
            Resolve<BenefitsExpertContext>().Database.Log = Console.Write;
        }

        public static void Transaction(Action<BenefitsExpertContext> action)
        {
            using (var database = new BenefitsExpertContext())
            {
                try
                {
                    database.BeginTransaction();
                    action(database);
                    database.CloseTransaction();
                }
                catch (Exception exception)
                {
                    database.CloseTransaction(exception);
                    throw;
                }
            }
        }

        public static void Save(params object[] entities)
        {
            Resolve<SaveAtMostOncePolicy>().Enforce();

            Transaction(database =>
            {
                foreach (var entity in entities)
                    database.Set(entity.GetType()).Add(entity);
            });
        }

        private class SaveAtMostOncePolicy
        {
            private bool _hasAlreadySaved;

            public void Enforce()
            {
                if (_hasAlreadySaved)
                    throw new InvalidOperationException(
                        "A test should call Save(...) at most once. Otherwise, " +
                        "it is likely that duplicate records will be created, as each " +
                        "call to this method uses a distinct DbContext.");

                _hasAlreadySaved = true;
            }
        }

        public static TResult Query<TResult>(Func<BenefitsExpertContext, TResult> query)
        {
            var result = default(TResult);

            Transaction(database =>
            {
                result = query(database);
            });

            return result;
        }

        public static IValidator Validator<TResult>(IRequest<TResult> message)
        {
            var validatorType = typeof(IValidator<>).MakeGenericType(message.GetType());

            return Container.TryGetInstance(validatorType) as IValidator;
        }

        public static IValidator Validator<TResult>(IAsyncRequest<TResult> message)
        {
            var validatorType = typeof(IValidator<>).MakeGenericType(message.GetType());

            return Container.TryGetInstance(validatorType) as IValidator;
        }

        public static void Send(IRequest message)
        {
            Send((IRequest<Unit>)message);
        }

        public static TResult Send<TResult>(IRequest<TResult> message)
        {
            var validator = Validator(message);

            if (validator != null)
                message.ShouldValidate();

            TResult result;

            var database = Resolve<BenefitsExpertContext>();
            try
            {
                database.BeginTransaction();
                result = Resolve<IMediator>().Send(message);
                database.CloseTransaction();
            }
            catch (Exception exception)
            {
                database.CloseTransaction(exception);
                throw;
            }

            return result;
        }

        public static async Task<TResult> SendAsync<TResult>(IAsyncRequest<TResult> message)
        {
            var validator = Validator(message);

            if (validator != null)
                message.ShouldValidate();

            TResult result;

            var database = Resolve<BenefitsExpertContext>();
            try
            {
                database.BeginTransaction();
                result = await Resolve<IMediator>().SendAsync(message);
                database.CloseTransaction();
            }
            catch (Exception exception)
            {
                database.CloseTransaction(exception);
                throw;
            }

            return result;
        }

        public static void LogIn(User user)
        {
            // Rather than store the given user object directly into the resolved UserContext,
            // we fetch an equivalent instance via the DirectoryContext. This way, the instance
            // that describes the current user will take part in the DirectoryContext's change
            // tracking, which better matches the production scenario.

            Resolve<UserContext>().User = Resolve<BenefitsExpertContext>().Users.Single(x => x.Id == user.Id);
        }
    }
}