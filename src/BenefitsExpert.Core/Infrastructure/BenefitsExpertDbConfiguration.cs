﻿namespace BenefitsExpert.Core.Infrastructure
{
    using System.Data.Entity;

    /// <summary>
    /// This class is automatically discovered and used by EntityFramework.
    /// </summary>
    public class DirectoryDbConfiguration : DbConfiguration
    {
        public DirectoryDbConfiguration()
        {
            DisableAutomaticDatabaseInitialization();
        }

        private void DisableAutomaticDatabaseInitialization()
        {
            SetDatabaseInitializer<BenefitsExpertContext>(null);
        }
    }
}