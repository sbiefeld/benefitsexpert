﻿namespace BenefitsExpert.Core
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Data.SqlClient;
    using Microsoft.Azure;
    using BenefitsExpert.Core.Infrastructure;
    using Domain;

    public class BenefitsExpertContext : DbContext
    {
        public static readonly string ConnectionString = DbConnString;

        private DbContextTransaction _currentTransaction;

        public BenefitsExpertContext()
            : base(ConnectionString)
        {
        }

        public static string DbConnString
        {
            get
            {
                if (AppSettings.Environment == "Prod" || AppSettings.Environment == "TEST" || AppSettings.Environment == "DEV")
                    return new SqlConnectionStringBuilder(CloudConfigurationManager.GetSetting("DBContext")).ToString();

                return new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["DBContext"].ConnectionString).ToString();
            }
        }

        public static class AppSettings
        {
            public static string Environment
            {
                get { return CloudConfigurationManager.GetSetting("Environment"); }
            }
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Deduction> Deductions { get; set; }
        public DbSet<Dependent> Dependents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            UseIdValuesGeneratedByTheDatabase(modelBuilder);
            UseSimpleForeignKeyNamingConvention(modelBuilder);
            UseSingularTableNames(modelBuilder);
            SearchThisAssemblyForEntityMappings(modelBuilder);
        }

        private static void UseIdValuesGeneratedByTheDatabase(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add<IdentityConvention>();
        }

        private static void UseSimpleForeignKeyNamingConvention(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add<ForeignKeyNamingConvention>();
        }

        private static void UseSingularTableNames(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        private static void SearchThisAssemblyForEntityMappings(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(typeof(BenefitsExpertContext).Assembly);
        }

        public void BeginTransaction()
        {
            if (_currentTransaction != null)
                return;

            _currentTransaction = Database.BeginTransaction(IsolationLevel.ReadCommitted);
        }

        public void CloseTransaction()
        {
            CloseTransaction(exception: null);
        }

        public void CloseTransaction(Exception exception)
        {
            try
            {
                if (_currentTransaction != null && exception != null)
                {
                    _currentTransaction.Rollback();
                    return;
                }

                SaveChanges();

                _currentTransaction?.Commit();
            }
            catch (Exception)
            {
                if (_currentTransaction?.UnderlyingTransaction.Connection != null)
                    _currentTransaction.Rollback();

                throw;
            }
            finally
            {
                _currentTransaction?.Dispose();
                _currentTransaction = null;
            }
        }
    }
}
