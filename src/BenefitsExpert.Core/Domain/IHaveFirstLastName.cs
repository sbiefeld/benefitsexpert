namespace BenefitsExpert.Core.Domain
{
    public interface IHaveFirstLastName
    {
        string FirstName { get; set; }
        string LastName { get; set; }
    }
}