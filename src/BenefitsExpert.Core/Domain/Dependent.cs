﻿namespace BenefitsExpert.Core.Domain
{
    using System;

    public class Dependent : Entity, IHaveFirstLastName
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Relationship { get; set; }
        public Guid UserId { get; set; }
        public User Employee { get; set; }
    }
}