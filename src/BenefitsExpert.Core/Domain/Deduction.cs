﻿namespace BenefitsExpert.Core.Domain
{
    using System.Collections.Generic;
    using System.Linq;
    using Extensions;

    public class Deduction : Entity
    {
        public decimal DeductionPerEmployeePerYear { get; set; }
        public decimal DependentDeductionPerYear { get; set; }
        public decimal DiscountPercentage { get; set; }
        public string NameStartsWithDiscountRule { get; set; }
        public decimal DefaultPayPerPaycheck { get; set; }
        public int PaychecksPerYear { get; set; }
        public int Year { get; set; }
        public virtual decimal EmployeeYearGross => (DefaultPayPerPaycheck * PaychecksPerYear);
        
        public decimal CalculateAllDependentsCostPerYear(IEnumerable<IHaveFirstLastName> dependents)
        {
            return dependents.Sum(d => ApplyDiscount(DependentDeductionPerYear, d));
        }

        public decimal CalculateTotalCostPerYear(IHaveFirstLastName employeeNames, IEnumerable<IHaveFirstLastName> dependents)
        {
            return ApplyDiscount(DeductionPerEmployeePerYear, employeeNames) + CalculateAllDependentsCostPerYear(dependents);
        }
        
        protected decimal ApplyDiscount(decimal amount, IHaveFirstLastName names)
        {
            var discountApplies = names.FirstName.MatchesDiscountRule(NameStartsWithDiscountRule) ||
                                  names.LastName.MatchesDiscountRule(NameStartsWithDiscountRule);

            return discountApplies ? amount * (1m - DiscountPercentage) : amount;
        }
    }
}