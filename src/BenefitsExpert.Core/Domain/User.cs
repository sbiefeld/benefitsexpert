﻿namespace BenefitsExpert.Core.Domain
{
    using System.Collections.Generic;

    public class User : Entity, IHaveFirstLastName
    {
        public User()
        {
            Dependents = new List<Dependent>();
        }

        public bool IsAdmin { get; set; }
        public string EmployeeNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public string Department { get; set; }
        public string Location { get; set; }
        public string Email { get; set; }
        public List<Dependent> Dependents { get; set; }
    }
}
