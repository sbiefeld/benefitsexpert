﻿namespace BenefitsExpert.Core.Domain
{
    public class DependentRelationship : Enumeration<DependentRelationship>
    {
        public static readonly DependentRelationship Child = new DependentRelationship(0, "Child");
        public static readonly DependentRelationship Spouse = new DependentRelationship(1, "Spouse");

        public DependentRelationship(int value, string displayName) : base(value, displayName)
        {
        }
    }
}