﻿namespace BenefitsExpert.Core.Domain
{
    using System;

    public interface IPersistentObject<T> : IPersistentObject
    {
        T Id { get; set; }
    }

    public interface IPersistentObject
    {
        bool IsPersistent { get; }
    }

    public abstract class Entity : Entity<Guid>
    {

    }

    public abstract class Entity<T> : IPersistentObject<T>, IEquatable<Entity<T>>
    {
        public virtual bool Equals(Entity<T> other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return Id.Equals(other.Id);
        }

        public T Id { get; set; }

        public bool IsPersistent
        {
            get { return !Equals(Id, default(T)); }
        }

        public override bool Equals(object obj)
        {
            if (IsPersistent)
            {
                var persistentObject = obj as IPersistentObject<T>;
                return persistentObject != null && Equals(Id, persistentObject.Id);
            }

            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return IsPersistent ? Id.GetHashCode() : base.GetHashCode();
        }

        public static bool operator ==(Entity<T> left,
            Entity<T> right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Entity<T> left,
            Entity<T> right)
        {
            return !Equals(left, right);
        }
    }
}