﻿namespace BenefitsExpert.Core.Extensions
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public static class TypeExtensions
    {
        public static T Cast<T>(this object target)
        {
            return (T)target;
        }

        public static bool Implements(
            this Type type,
            Type @interface)
        {
            return type.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == @interface);
        }

        public static bool IsAssignableTo<T>(this Type type)
        {
            return typeof(T).IsAssignableFrom(type);
        }

        public static bool IsAssignableTo(this Type type, Type target)
        {
            return target.IsAssignableFrom(type);
        }

        public static bool IsDefaultEmptyOrNull<T>(this T value)
        {
            if (ReferenceEquals(value, null))
            {
                return true;
            }

            var type = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);

            var finalValue = Convert.ChangeType(value, type);

            if (type == typeof(string))
            {
                return string.IsNullOrEmpty(finalValue as string);
            }

            if (type == typeof(ICollection))
            {
                var collection = finalValue as ICollection;
                return collection != null && collection.Count == 0;
            }

            return finalValue.Equals(default(T));
        }

        public static bool IsAssignableToGenericType(this Type givenType, Type genericType)
        {
            Type[] interfaceTypes = givenType.GetInterfaces();

            if (interfaceTypes.Any(it => it.IsGenericType && it.GetGenericTypeDefinition() == genericType))
            {
                return true;
            }

            if (givenType.IsGenericType && givenType.GetGenericTypeDefinition() == genericType)
            {
                return true;
            }

            Type baseType = givenType.BaseType;
            if (baseType == null)
            {
                return false;
            }

            return IsAssignableToGenericType(baseType, genericType);
        }
        
        public static bool IsIEnumerable(this Type type)
        {
            return type
                    .GetInterfaces()
                    .Any(t => t.IsGenericType
                              && t.GetGenericTypeDefinition() == typeof(IEnumerable<>));
        }
    }
}