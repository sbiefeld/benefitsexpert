﻿namespace BenefitsExpert.Core.Extensions
{
    using System;

    public static class StringExtensions
    {
        public static bool MatchesDiscountRule(this string name, string value)
        {
            return name.Trim().StartsWith(value.Trim(), StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
