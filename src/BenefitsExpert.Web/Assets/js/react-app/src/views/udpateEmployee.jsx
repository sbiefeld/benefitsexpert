﻿import React from 'react';
import ReactDOM from 'react-dom';
import JsonTable from 'react-json-table';
import Reqwest from 'reqwest';
import _ from 'lodash';
import AjaxErrors from '../core/AjaxErrors.js';

_.mixin({
    guid : function(){
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }
});

class UpdateEmployee extends React.Component {
    constructor() {
        super();
        this.state = {
            employee: window.global_data.model
        };
    }

    handleChange(field, e) {
        var employee = this.state.employee;
        employee[field] = e.target.value;
        this.updateCalculations.call(this, employee, field);
        this.setState({employee})
    }

    updateState(field, value){
        var employee = this.state.employee;
        employee[field] = value;
        this.setState({employee})
    }

    handleCheckChange(field, e) {
        this.updateState(field, e.target.checked);
    }

    handleCalculationUpdate(benefitInfo){
        this.updateState('benefitInfo', benefitInfo);
    }

    updateCalculations(employee, field){
        if(field === 'firstName' || field === 'lastName' || field === 'dependents'){
            var view = this;
            Reqwest({
                url: '/Benefits/Cost',
                method: 'post',
                type: 'json',
                data: { 
                    firstName: employee.firstName,
                    lastName: employee.lastName,
                    year: new Date().getUTCFullYear(),
                    dependents: employee.dependents
                },
                error: (error) => {
                    console.error(error);
                },
                success: (response) => {
                    this.handleCalculationUpdate.call(this, response);
                }
            });
        }
    }

    handleSave(e) {
        Reqwest({
            url: '/Employees/Update',
            method: 'post',
            type: 'json',
            data: this.state.employee,
            error: (xhr, msg) => {
                if (xhr.status === 400) {
                    AjaxErrors.handleServerErrors(JSON.parse(xhr.responseText));
                }
            },
            success: (resp) => {                
                window.location = "/Employees";
            }
        });
    }

    handleDependentRemoval(dependentId, altKey){
        let employee = this.state.employee;
        const field = 'dependents';
        this.updateState(field, _.filter(employee.dependents, (dependent) => {
            return dependent.id !== dependentId;
        }));
        this.updateCalculations.call(this, employee, field);
    }

    handleCreateDependentClicked(){
        var row = document.querySelector('#addDependentRow');
        if(row){
            row.className = '';
        }
    }

    handleDependentCreation(e){
        let employee = this.state.employee;
        const field = 'dependents';
        const parent = e.target.parentElement.parentElement;
        const relationshipEl = parent.querySelector('#dependentRelationship'),
            firstNameEL = parent.querySelector('#dependentFirstName'),
            lastNameEl = parent.querySelector('#dependentLastName');
        employee.dependents.push(
            {
                id: _.guid(),
                firstName: firstNameEL.value,
                lastName: lastNameEl.value,
                relationship: relationshipEl.value,
                relationshipName: relationshipEl.options[relationshipEl.selectedIndex].text
            });
        this.updateState(field, employee.dependents);
        this.updateCalculations.call(this, employee, field);
        parent.className = 'hidden';
        lastNameEl.value = '';
        firstNameEL.value = '';
    }
    
    renderDependentRow(dependent, altKey){
        return (
            <tr key={dependent.id || altKey}>
                <td>{dependent.firstName}</td>
                <td>{dependent.lastName}</td>
                <td>{dependent.relationshipName}</td>
                <td><button onClick={this.handleDependentRemoval.bind(this, dependent.id, altKey)}>Remove</button></td>
            </tr>
        );
    }

    render() {
        console.debug(this.state);
        let imageUri = 'https://robohash.org/' + this.state.employee.email + '?set=set3&size=100x100';
        
        let isAdminChecked = this.state.employee.isAdmin 
            ? (<input type="checkbox" id="isAdmin" value={this.state.employee.isAdmin} onChange={this.handleCheckChange.bind(this, 'isAdmin')} checked="checked" />)
            : (<input type="checkbox" id="isAdmin" value={this.state.employee.isAdmin} onChange={this.handleCheckChange.bind(this, 'isAdmin')} />);

        let dependents = this.state.employee.dependents.map((dependent, i) => {
            return this.renderDependentRow(dependent);
        });
        return(
            <div className="container">
                <div className="section twelve columns">
                    <div className="two columns">
                        <div className="employee-photo">
                            <img src={imageUri} />
                        </div>
                    </div>
                    <div className="ten columns employee-name-title">
                        <h3 className="vertical-align">{this.state.employee.lastName}, {this.state.employee.firstName}</h3>
                    </div>
                </div>
                <form>
                    <div className="section twelve columns">
                        <div className="row">
                            <h4 className="legend">Personal Info</h4>
                        </div>
                        <div className="row">
                            <div className="six columns">
                                <label htmlFor="firstName">First Name</label>
                                <input className="u-full-width" type="text" placeholder="first name..." id="firstName" value={this.state.employee.firstName} onChange={this.handleChange.bind(this, 'firstName')} />
                            </div>
                            <div className="six columns">
                                <label htmlFor="lastName">Last Name</label>
                                <input className="u-full-width" type="text" placeholder="last name..." id="lastName" value={this.state.employee.lastName} onChange={this.handleChange.bind(this, 'lastName')} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="six columns">
                                <label htmlFor="email">Email</label>
                                <input className="u-full-width" type="email" placeholder="test@test.com" id="email" value={this.state.employee.email} onChange={this.handleChange.bind(this, 'email')} />
                            </div>
                        </div>
                    </div>
                    <div className="section twelve columns">
                        <div className="row">
                            <h4 className="legend">Corporate Info</h4>
                        </div>
                        <div className="row">
                            <div className="six columns">
                                <label htmlFor="employeeNumber">Employee Number</label>
                                <p className="u-full-width" type="text" id="employeeNumber" >{this.state.employee.employeeNumber}</p>
                            </div>
                            <div className="six columns">
                                <label htmlFor="jobTitle">Job Title</label>
                                <input className="u-full-width" type="text" placeholder="title..." id="jobTitle" value={this.state.employee.jobTitle} onChange={this.handleChange.bind(this, 'jobTitle')} />
                            </div>
                        </div>
                        <div className="row">                            
                            <div className="six columns">
                                <label htmlFor="department">Department</label>
                                <input className="u-full-width" type="text" placeholder="department..." id="department" value={this.state.employee.department} onChange={this.handleChange.bind(this, 'department')} />
                            </div>
                            <div className="six columns">
                                <label htmlFor="location">Location</label>
                                <input className="u-full-width" type="text" placeholder="location..." id="location" value={this.state.employee.location} onChange={this.handleChange.bind(this, 'location')} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="twelve columns">
                                <label className="isAdmin" htmlFor="isAdmin"> 
                                    {isAdminChecked}
                                    <span className="label-body">Is Admin</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </form>
                <div className="section twelve columns">
                    <div className="row">
                        <h4 className="legend">Benefit Analysis</h4>
                    </div>
                    <div className="row">
                        <div className="six columns">
                            <label htmlFor="totalCostPerYear">Total Cost Per Year</label>
                            <div className="u-full-width" id="totalCostPerYear">$ {this.state.employee.benefitInfo.totalCostPerYear}</div>
                        </div>
                        <div className="six columns">
                            <label htmlFor="dependentCostPerYear">Dependents Cost Per Year</label>
                            <div className="u-full-width" id="dependentCostPerYear">$ {this.state.employee.benefitInfo.dependentCostPerYear}</div>
                        </div>
                    </div>
                </div>
                <div className="section twelve columns">
                    <div className="row">
                        <h4 className="legend">Dependents</h4>
                    </div>
                    <div className="row">
                        <button className="u-full-width" onClick={this.handleCreateDependentClicked.bind(this)} >Create Dependent</button>
                    </div>
                    <div className="row">
                        <table id="dependents" className="u-full-width">
                            <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Relationship</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="addDependentRow" className="hidden">
                                    <td><input className="u-full-width" type="text" placeholder="first name..." id="dependentFirstName" /></td>
                                    <td><input className="u-full-width" type="text" placeholder="last name..." id="dependentLastName" /></td>
                                    <td><select className="u-full-width" id="dependentRelationship">
                                        <option value="0">Child</option>
                                        <option value="1">Spouse</option>
                                        </select></td>
                                    <td><button onClick={this.handleDependentCreation.bind(this)}>Create</button></td>
                                </tr>
                                {dependents}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="twelve columns section">
                        <div className="row">
                            <div className="six columns">
                                <a className="button u-full-width" href="/Employees">Back</a>
                            </div>
                            <div className="six columns">
                                <button className="button-primary u-full-width" onClick={this.handleSave.bind(this)} >Save</button>
                            </div>
                        </div>
                    </div>
            </div>
        );
                            }
                            }
const componentDom = document.getElementById('react-update-employee');

if (componentDom) {
    ReactDOM.render(<UpdateEmployee />, componentDom);
                            }

export default UpdateEmployee;
