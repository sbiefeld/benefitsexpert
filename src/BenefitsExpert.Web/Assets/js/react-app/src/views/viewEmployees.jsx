﻿import React from 'react';
import ReactDOM from 'react-dom';
import JsonTable from 'react-json-table';

class ViewEmployees extends React.Component {
    constructor() {
        super();
        this.state = {
            employees: window.global_data.model,
            columns: [
                {key: 'employeeNumber', label: 'Emp. #'},
                {key: 'firstName', label: 'Name'},
                {key: 'lastName', label: 'Surname'},
                {key: 'department', label: 'Dept.'},
                {key: 'jobTitle', label: 'Title'},
                {key: 'location', label: 'Location'},
                {key: 'id', label: ' ', cell: function( item, columnKey ){
                    const uri = "/Employees/Update/" + item.id;
                    return <a className="button" href={ uri }>View</a>;
                }}
            ]
        };
    }
    render() {
        return(<div className="twelve columns"><h2>Employees </h2><a className="button u-full-width" href="/Employees/Create">Create Employee</a><JsonTable rows={ this.state.employees } columns={ this.state.columns } /></div>);
    }
}
const componentDom = document.getElementById('react-view-employees');

    if (componentDom) {
        ReactDOM.render(<ViewEmployees/>, componentDom);
    }

export default ViewEmployees;
