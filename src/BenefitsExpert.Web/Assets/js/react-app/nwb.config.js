module.exports = {
  // Let nwb know this is a React app when generic build commands are used
    type: 'react-app',
    module: {
        loaders: [
          { test: /\.css$/, loader: "style-loader!css-loader" },
          { test: /\.png$/, loader: "url-loader?limit=100000" },
          { test: /\.jpg$/, loader: "file-loader" }
        ]
    }
}
