﻿namespace BenefitsExpert
{
    using System.Web.Mvc;
    using Infrastructure;
    using Infrastructure.Validation;

    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new ValidatorActionFilter());
            filters.Add(new UserContextFilter());
            filters.Add(new UnitOfWork());
            filters.Add(new AuthenticationActionFilter());
            filters.Add(new SessionExpireAttribute());
        }
    }
}
