﻿namespace BenefitsExpert.Features
{
    using System.Web.Mvc;
    using Account;
    using AutoMapper;
    using Infrastructure;

    public static class HtmlExtensions
    {
        public static bool IsDebug(this HtmlHelper htmlHelper)
        {
            #if DEBUG
                return true;
            #else
                return false;
            #endif
        }

        public static CurrentUser GetCurrentUser(this HtmlHelper htmlHelper)
        {
            var userContext = DependencyResolver.Current.GetService<UserContext>();
            var mapper = DependencyResolver.Current.GetService<IMapper>();

            return mapper.Map<Core.Domain.User, CurrentUser>(userContext.User);
        }

        public static string GetUserImageUri(this HtmlHelper htmlHelper, string email, int size)
        {
            return $"https://robohash.org/{email}?set=set3&size={size}x{size}";
        }
    }
}