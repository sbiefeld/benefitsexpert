﻿namespace BenefitsExpert.Features.Employees
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Infrastructure;
    using MediatR;

    public class EmployeesController : Controller
    {
        private readonly IMediator mediator;
        public EmployeesController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public async Task<ActionResult> Index()
        {
            return View(await mediator.SendAsync(new Employee.QueryAll()));
        }

        public ActionResult Create()
        {
            return View();
        }

        public async Task<ActionResult> Update(Guid id)
        {
            return View(await mediator.SendAsync(new Update.Query(id)));
        }

        [HttpPost]
        public JsonNetResult Create(Create.Command command)
        {
            var response = mediator.Send(command);
            return new JsonNetResult(response);
        }

        [HttpPost]
        public JsonNetResult Update(Update.Command command)
        {
            var response = mediator.Send(command);
            return new JsonNetResult(response);
        }
    }
}