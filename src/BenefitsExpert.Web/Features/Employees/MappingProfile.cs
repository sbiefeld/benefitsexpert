﻿namespace BenefitsExpert.Features.Employees
{
    using AutoMapper;
    using BenefitsExpert.Features.Employees;
    using Core.Domain;

    public class MappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<User, Employee.Model>();
            CreateMap<User, Update.Command>();
            CreateMap<Dependent, Employee.DependentModel>();
        }
    }
}