﻿namespace BenefitsExpert.Features.Employees
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using Benefits;
    using Core;
    using Core.Domain;
    using FluentValidation;
    using MediatR;
    using EntityFramework;

    public class Update
    {
        public class Command : IRequest
        {
            public Command()
            {
                Dependents = new List<Employee.DependentModel>();
            }

            public Guid Id { get; set; }
            public bool IsAdmin { get; set; }
            public string EmployeeNumber { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string JobTitle { get; set; }
            public string Department { get; set; }
            public string Location { get; set; }
            public string Email { get; set; }
            public List<Employee.DependentModel> Dependents { get; set; }
            [IgnoreMap]
            public CalculateCostsResponse BenefitInfo { get; set; }
        }

        public class Validator : AbstractValidator<Command>
        {
            private readonly BenefitsExpertContext dbContext;

            public Validator(BenefitsExpertContext dbContext)
            {
                this.dbContext = dbContext;
                RuleFor(m => m.Id)
                    .NotEmpty()
                    .MustAsync(EmployeeIdMustExist)
                    .WithMessage("The Employee specified does not exist.");

                RuleFor(m => m.EmployeeNumber)
                    .NotEmpty()
                    .MustAsync(EmployeeNumberMustBeUnique)
                    .WithMessage("Employee Number must be unique.");

                RuleFor(m => m.Email)
                    .NotEmpty()
                    .EmailAddress()
                    .MustAsync(EmailMustBeUnique)
                    .WithMessage("Email must be unique.");

                RuleFor(m => m.FirstName).NotEmpty();
                RuleFor(m => m.LastName).NotEmpty();
                RuleFor(m => m.JobTitle).NotEmpty();
            }

            private Task<bool> EmployeeIdMustExist(Guid id, CancellationToken cancellationToken)
            {
                return dbContext.Users.AnyAsync(u => u.Id == id, cancellationToken);
            }

            private Task<bool> EmployeeNumberMustBeUnique(Command command, string employeeNumber, CancellationToken cancellationToken)
            {
                return dbContext.Users.AllAsync(u => u.EmployeeNumber != employeeNumber || u.Id == command.Id, cancellationToken);
            }

            private Task<bool> EmailMustBeUnique(Command command, string email, CancellationToken cancellationToken)
            {
                return dbContext.Users.AllAsync(u => u.Email != email || u.Id == command.Id, cancellationToken);
            }
        }

        public class Handler : RequestHandler<Command>
        {
            private readonly BenefitsExpertContext dbContext;

            public Handler(BenefitsExpertContext dbContext)
            {
                this.dbContext = dbContext;
            }

            protected override void HandleCore(Command message)
            {
                var user = dbContext.Users.Include(u => u.Dependents).Single(u => u.Id == message.Id);
                user.Email = message.Email;
                user.FirstName = message.FirstName;
                user.LastName = message.LastName;
                user.Department = message.Department;
                user.EmployeeNumber = message.EmployeeNumber;
                user.IsAdmin = message.IsAdmin;
                user.JobTitle = message.JobTitle;
                user.Location = message.Location;
                MapDependents(message.Dependents, user);
            }

            private void MapDependents(List<Employee.DependentModel> dependents, User employee)
            {
                dbContext.Dependents.RemoveRange(employee.Dependents);

                if (dependents != null)
                {
                    foreach (var dependent in dependents)
                    {
                        employee.Dependents.Add(new Dependent
                        {
                            Relationship = dependent.Relationship,
                            Employee = employee,
                            FirstName = dependent.FirstName,
                            LastName = dependent.LastName
                        });
                    }
                }
            }
        }

        public class Query : IAsyncRequest<Command>
        {
            public Guid Id { get; set; }

            public Query(Guid id)
            {
                Id = id;
            }
        }

        public class QueryHandler : IAsyncRequestHandler<Query, Command>
        {
            private readonly BenefitsExpertContext dbContext;
            private readonly MapperConfiguration mapperConfiguration;
            private readonly IMediator mediator;

            public QueryHandler(BenefitsExpertContext dbContext, MapperConfiguration mapperConfiguration, IMediator mediator)
            {
                this.dbContext = dbContext;
                this.mapperConfiguration = mapperConfiguration;
                this.mediator = mediator;
            }

            public async Task<Command> Handle(Query message)
            {
                var employee = await dbContext.Users
                    .Where(u => u.Id == message.Id)
                    .ProjectToSingleAsync<Command>(mapperConfiguration);

                employee.BenefitInfo = await mediator.SendAsync(new CalculateCosts
                {
                    FirstName = employee.FirstName,
                    LastName = employee.LastName,
                    Year = DateTime.Now.Year,
                    Dependents = employee.Dependents.Select(d => new DependentModel
                    {
                        FirstName = d.FirstName,
                        LastName = d.LastName
                    }).ToList()
                });

                return employee;
            }
        }
    }
}