﻿namespace BenefitsExpert.Features.Employees
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Core;
    using Core.Domain;
    using FluentValidation;
    using MediatR;

    public class Create
    {
        public class Command : IRequest
        {
            public bool IsAdmin { get; set; }
            public string EmployeeNumber { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string JobTitle { get; set; }
            public string Department { get; set; }
            public string Location { get; set; }
            public string Email { get; set; }
            public List<Employee.DependentModel> Dependents { get; set; }
        }

        public class Validator : AbstractValidator<Command>
        {
            private readonly BenefitsExpertContext dbContext;

            public Validator(BenefitsExpertContext dbContext)
            {
                this.dbContext = dbContext;
                RuleFor(m => m.EmployeeNumber)
                    .NotEmpty()
                    .MustAsync(EmployeeNumberMustBeUnique)
                    .WithMessage("Employee Number must be unique.");

                RuleFor(m => m.Email)
                    .EmailAddress()
                    .NotEmpty()
                    .MustAsync(EmailMustBeUnique)
                    .WithMessage("Email must be unique.");

                RuleFor(m => m.FirstName).NotEmpty();
                RuleFor(m => m.LastName).NotEmpty();
                RuleFor(m => m.JobTitle).NotEmpty();
            }

            private Task<bool> EmployeeNumberMustBeUnique(string employeeNumber, CancellationToken cancellationToken)
            {
                return dbContext.Users.AllAsync(u => u.EmployeeNumber != employeeNumber, cancellationToken);
            }

            private Task<bool> EmailMustBeUnique(string email, CancellationToken cancellationToken)
            {
                return dbContext.Users.AllAsync(u => u.Email != email, cancellationToken);
            }
        }

        public class Handler : RequestHandler<Command>
        {
            private readonly BenefitsExpertContext dbContext;

            public Handler(BenefitsExpertContext dbContext)
            {
                this.dbContext = dbContext;
            }

            protected override void HandleCore(Command message)
            {
                var user = new User
                {
                    Email = message.Email,
                    FirstName = message.FirstName,
                    LastName = message.LastName,
                    Department = message.Department,
                    EmployeeNumber = message.EmployeeNumber,
                    IsAdmin = message.IsAdmin,
                    JobTitle = message.JobTitle,
                    Location = message.Location
                };
                user.Dependents = MapDependents(message.Dependents, user);
                dbContext.Users.Add(user);
            }

            private static List<Dependent> MapDependents(List<Employee.DependentModel> dependents, User employee)
            {
                return dependents.Select(m => new Dependent
                {
                    Relationship = m.Relationship,
                    Employee = employee,
                    FirstName = m.FirstName,
                    LastName = m.LastName
                }).ToList();
            }
        }
    }
}