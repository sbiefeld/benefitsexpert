﻿namespace BenefitsExpert.Features.Employees
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AutoMapper;
    using Core;
    using Core.Domain;
    using MediatR;

    public class Employee
    {
        public class Model : IHaveFirstLastName
        {
            public Guid Id { get; set; }
            public bool IsAdmin { get; set; }
            public string EmployeeNumber { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string JobTitle { get; set; }
            public string Department { get; set; }
            public string Location { get; set; }
            public string Email { get; set; }
        }

        public class DependentModel : IHaveFirstLastName
        {
            public Guid Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public int Relationship { get; set; }
            [IgnoreMap]
            public string RelationshipName => DependentRelationship.FromValue(Relationship).DisplayName;
            public Guid EmployeeId { get; set; }
        }

        public class QueryAll : IAsyncRequest<List<Model>>
        {

        }

        public class QueryAllHandler : IAsyncRequestHandler<QueryAll, List<Model>>
        {
            private readonly BenefitsExpertContext dbContext;
            private readonly MapperConfiguration mapperConfig;

            public QueryAllHandler(BenefitsExpertContext dbContext, MapperConfiguration mapperConfig)
            {
                this.dbContext = dbContext;
                this.mapperConfig = mapperConfig;
            }

            public Task<List<Model>> Handle(QueryAll message)
            {
                return dbContext.Users.ProjectToListAsync<Model>(mapperConfig);
            }
        }
    }
}