﻿namespace BenefitsExpert.Features.Account
{
    using System;

    public class CurrentUser
    {
        public Guid Id { get; set; }
        public string EmployeeNumber { get; set; }
        public string DisplayName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public string Department { get; set; }
        public string Location { get; set; }
        public string Email { get; set; }
    }
}