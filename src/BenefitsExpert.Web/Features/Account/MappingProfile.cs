﻿namespace BenefitsExpert.Features.Account
{
    using AutoMapper;
    using Core.Domain;

    public class MappingProfile : Profile
    {
        protected override void Configure()
        {
            this.CreateMap<User, CurrentUser>();
        }
    }
}