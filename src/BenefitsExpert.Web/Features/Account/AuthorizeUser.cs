﻿namespace BenefitsExpert.Features.Account
{
    using System.Linq;
    using Core;
    using Core.Domain;
    using MediatR;

    public class AuthorizeUser : IRequest<bool>
    {
        public string EmailAddress { get; set; }
    }

    public class AuthorizeUserHandler : IRequestHandler<AuthorizeUser, bool>
    {
        private readonly BenefitsExpertContext context;

        public AuthorizeUserHandler(BenefitsExpertContext context)
        {
            this.context = context;
        }

        public bool Handle(AuthorizeUser message)
        {
            var user = context.Users.SingleOrDefault(u => u.Email == message.EmailAddress);
            if (user == null)
            {
                context.Users.Add(new User
                {
                    Email = message.EmailAddress,
                    IsAdmin = false
                });
            }
            return true;
        }
    }
}