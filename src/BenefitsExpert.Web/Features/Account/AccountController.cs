namespace BenefitsExpert.Features.Account {
    using System.Web.Mvc;
    using System.Web.Security;
    using Infrastructure;
    using MediatR;
    using Microsoft.Web.WebPages.OAuth;

    public class AccountController : Controller
    {
        private readonly IMediator mediator;

        public AccountController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        // GET: /Account/Logout
        public ActionResult Logout() {
            FormsAuthentication.SignOut();
            return this.Redirect("https://www.google.com/accounts/Logout");
        }

		[AllowAnonymous]
		public ActionResult Login()
		{
			GooglePlusClient.RewriteRequest();

			var result = OAuthWebSecurity.VerifyAuthentication(Url.Action("Login", new { ReturnUrl = "" }));

		    if (!result.IsSuccessful || !mediator.Send(new AuthorizeUser {EmailAddress = result.UserName}))
			{
			    return View("Unauthorized");
			}

            FormsAuthentication.SetAuthCookie(result.UserName, false);
            FormsAuthentication.RedirectFromLoginPage(result.UserName, false);

			return RedirectToAction("Index", "Employees");
		}
    }
}