﻿namespace BenefitsExpert.Features.Benefits
{
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Infrastructure;
    using MediatR;

    public class BenefitsController : Controller
    {
        private readonly IMediator mediator;
        public BenefitsController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        public async Task<JsonNetResult> Cost(CalculateCosts query)
        {
            return new JsonNetResult(await mediator.SendAsync(query));
        }
    }
}