﻿namespace BenefitsExpert.Features.Benefits
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Threading.Tasks;
    using Core;
    using Core.Domain;
    using FluentValidation;
    using MediatR;

    public class CalculateCosts : IAsyncRequest<CalculateCostsResponse>, IHaveFirstLastName
    {
        public int Year { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<DependentModel> Dependents { get; set; }
    }

    public class DependentModel : IHaveFirstLastName
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class CalculateCostsValidator : AbstractValidator<CalculateCosts>
    {
        public static readonly string EmployeeMustExistMessage =
            "The employee information provided does not match any existing employee.";

        public CalculateCostsValidator()
        {
            RuleFor(m => m.Year).GreaterThan(0);
        }
    }

    public class CalculateCostsHandler : IAsyncRequestHandler<CalculateCosts, CalculateCostsResponse>
    {
        private readonly BenefitsExpertContext dbContext;
        public CalculateCostsHandler(BenefitsExpertContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<CalculateCostsResponse> Handle(CalculateCosts message)
        {
            var deductions = await dbContext.Deductions.SingleOrDefaultAsync(b => b.Year == message.Year);

            if (message.Dependents == null)
            {
                message.Dependents = new List<DependentModel>();
            }

            var dependentsCount = message.Dependents.Count;

            return new CalculateCostsResponse
            {
                DependentCount = dependentsCount,
                TotalCostPerYear = deductions.CalculateTotalCostPerYear(message, message.Dependents).RoundWithPrecision(2),
                DependentCostPerYear = deductions.CalculateAllDependentsCostPerYear(message.Dependents).RoundWithPrecision(2)
            };
        }
    }

    public static class DeductionDecimalExtensions
    {
        public static decimal RoundWithPrecision(this decimal amount, int precision)
        {
            return Math.Round(amount, precision);
        }
    }

    public class CalculateCostsResponse
    {
        public decimal TotalCostPerYear { get; set; }
        public decimal DependentCostPerYear { get; set; }
        public decimal DependentCount { get; set; }
    }
}