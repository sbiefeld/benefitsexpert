﻿namespace BenefitsExpert.Infrastructure
{
    using System.Linq;
    using System.Web.Mvc;
    using Core;

    public class UserContextFilter : IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var dbContext = DependencyResolver.Current.GetService<BenefitsExpertContext>();
            var context = DependencyResolver.Current.GetService<UserContext>();

            if (filterContext != null)
            {
                var username = filterContext.HttpContext.User.Identity.Name;
                if (dbContext != null)
                {
                    var user = dbContext.Users.SingleOrDefault(x => x.Email == username);

                    if (context != null) context.User = user;
                }
            }
        }
    }
}