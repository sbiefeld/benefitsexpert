﻿namespace BenefitsExpert.Infrastructure.Validation
{
    using System.Web.Helpers;
    using System.Web.Mvc;

    public class ValidateAntiForgeryTokenOnPostActions : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.HttpMethod == "POST")
                AntiForgery.Validate();
        }
    }
}