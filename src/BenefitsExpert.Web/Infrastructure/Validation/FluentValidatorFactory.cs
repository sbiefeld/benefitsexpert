﻿namespace BenefitsExpert.Infrastructure.Validation
{
    using System;
    using System.Web.Mvc;
    using FluentValidation;

    public class FluentValidatorFactory : ValidatorFactoryBase
    {
        public override IValidator CreateInstance(Type validatorType)
            => DependencyResolver.Current.GetService(validatorType) as IValidator;
    }
}