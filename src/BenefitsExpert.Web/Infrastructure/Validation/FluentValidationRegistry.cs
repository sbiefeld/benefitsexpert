﻿namespace BenefitsExpert.Infrastructure.Validation
{
    using StructureMap.Configuration.DSL;

    public class FluentValidationRegistry : Registry
    {
        public FluentValidationRegistry()
        {
            FluentValidation.AssemblyScanner.FindValidatorsInAssemblyContaining<FluentValidationRegistry>()
                .ForEach(result =>
                    For(result.InterfaceType)
                        .Use(result.ValidatorType));
        }
    }
}