﻿namespace BenefitsExpert.Infrastructure
{
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;

    public class SessionExpireAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext currentContext = HttpContext.Current;
            HttpCookie authCookie = currentContext.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                // check  sessions here
                if (ticket != null && ticket.Expired)
                {
                    FormsAuthentication.SignOut();
                    filterContext.Result = new RedirectResult("https://www.google.com/accounts/Logout");
                    return;
                }
            }
            
            base.OnActionExecuting(filterContext);
        }
    }
}