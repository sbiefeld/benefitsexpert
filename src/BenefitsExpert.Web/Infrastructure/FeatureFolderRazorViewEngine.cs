﻿namespace BenefitsExpert.Infrastructure
{
    using System.Web.Mvc;

    public class FeatureFolderRazorViewEngine : RazorViewEngine
    {
        public FeatureFolderRazorViewEngine()
        {
            ViewLocationFormats = new[]
            {
                "~/Features/{1}/{0}.cshtml",
                "~/Features/Shared/{0}.cshtml",
            };

            MasterLocationFormats = ViewLocationFormats;

            PartialViewLocationFormats = new[]
            {
                "~/Features/{1}/{0}.cshtml",
                "~/Features/Shared/{0}.cshtml",
            };
        }
    }
}