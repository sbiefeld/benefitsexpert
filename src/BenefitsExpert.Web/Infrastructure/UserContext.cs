﻿namespace BenefitsExpert.Infrastructure
{
    using Core.Domain;

    public class UserContext
    {
        public User User { get; set; }
    }
}