﻿namespace BenefitsExpert.Infrastructure
{
    using System.Web.Mvc;
    using Core;

    public class UnitOfWork : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var database = DependencyResolver.Current.GetService<BenefitsExpertContext>();

            database.BeginTransaction();
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var database = DependencyResolver.Current.GetService<BenefitsExpertContext>();

            database.CloseTransaction(filterContext.Exception);
        }
    }
}