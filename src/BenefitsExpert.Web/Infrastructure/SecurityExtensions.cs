namespace BenefitsExpert.Infrastructure
{
    using System.Web;
    using System.Web.Mvc;
    using Core.Domain;

    public static class SecurityExtensions
    {
        public static User User(this HttpRequestBase request)
            => UserContext().User;

        public static bool HasPermission(this HttpRequestBase request)
            => UserContext().User.IsAdmin;

        private static UserContext UserContext()
            => DependencyResolver.Current.GetService<UserContext>();
    }
}
