﻿namespace BenefitsExpert.Web
{
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using FluentValidation.Mvc;
    using Infrastructure;
    using Infrastructure.Validation;

    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            EnableFeatureFolders();
            InitializeFluentValidation();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
        }

        private static void EnableFeatureFolders()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new FeatureFolderRazorViewEngine());

            ControllerBuilder.Current.SetControllerFactory(new FeatureFolderControllerFactory());
        }

        private static void InitializeFluentValidation()
        {
            FluentValidationModelValidatorProvider.Configure(provider => provider.ValidatorFactory = new FluentValidatorFactory());
        }
    }
}
