﻿IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'User'))
BEGIN
	IF NOT EXISTS(SELECT * 
	    FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
	    WHERE CONSTRAINT_NAME='uc_User_EmployeeNumber' )
	BEGIN
		ALTER TABLE [dbo].[User]
		ADD CONSTRAINT uc_User_EmployeeNumber UNIQUE (EmployeeNumber)
	END
END