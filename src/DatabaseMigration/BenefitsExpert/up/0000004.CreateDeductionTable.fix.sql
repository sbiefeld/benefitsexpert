﻿IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Deduction'))
BEGIN
	CREATE TABLE [dbo].[Deduction](
		[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Deduction_Id] DEFAULT NEWSEQUENTIALID() CONSTRAINT [PK_Deduction] PRIMARY KEY,
		[DeductionPerEmployeePerYear] [decimal](18,5) NOT NULL,
		[DependentDeductionPerYear] [decimal](18,5) NOT NULL,
		[DiscountPercentage] [decimal](18,5) NOT NULL,
		[NameStartsWithDiscountRule] [nvarchar](150) NULL,
		[DefaultPayPerPaycheck] [decimal](18,5) NOT NULL,
		[PaychecksPerYear] [int] NOT NULL,
		[Year] [int] NOT NULL)

	IF NOT EXISTS(SELECT * 
	    FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
	    WHERE CONSTRAINT_NAME='uc_Deduction_Year' )
	BEGIN
		ALTER TABLE [dbo].[Deduction]
		ADD CONSTRAINT uc_Deduction_Year UNIQUE (Year)
	END
END
go
