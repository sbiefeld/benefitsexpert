﻿IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'User'))
BEGIN
	CREATE TABLE [dbo].[User](
		[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_User_Id] DEFAULT NEWSEQUENTIALID() CONSTRAINT [PK_User] PRIMARY KEY,
		[EmployeeNumber] [nvarchar](150) NULL,
		[FirstName] [nvarchar](50) NULL,
		[LastName] [nvarchar](50) NULL,
		[JobTitle] [nvarchar](50) NULL,
		[Department] [nvarchar](50) NULL,
		[Location] [nvarchar](50) NULL,
		[Email] [nvarchar](450) NULL,
		[IsAdmin] [Bit] NOT NULL CONSTRAINT [DF_User_IsAdmin] DEFAULT 0,
		[UpdatedTimestamp] [datetime] NULL,
		[UpdatedBy] [nvarchar](256) NULL,
		[CreatedTimestamp] [datetime] NULL,
		[CreatedBy] [nvarchar](256) NULL)

	IF NOT EXISTS(SELECT * 
	    FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
	    WHERE CONSTRAINT_NAME='uc_User_Email' )
	BEGIN
		ALTER TABLE [dbo].[User]
		ADD CONSTRAINT uc_User_Email UNIQUE (Email)
	END
END
go
