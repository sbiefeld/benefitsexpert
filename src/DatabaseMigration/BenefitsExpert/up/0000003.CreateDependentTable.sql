﻿IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Dependent'))
BEGIN
	CREATE TABLE [dbo].[Dependent](
		[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Dependent_Id] DEFAULT NEWSEQUENTIALID() CONSTRAINT [PK_Dependent] PRIMARY KEY,
		[UserId] [uniqueidentifier] NOT NULL,
		[FirstName] [nvarchar](50) NULL,
		[LastName] [nvarchar](50) NULL,
		[Relationship] [int] NOT NULL)

	IF NOT EXISTS(SELECT * 
	    FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
	    WHERE CONSTRAINT_NAME='uc_Dependent_Id_UserId' )
	BEGIN
		ALTER TABLE [dbo].[Dependent]
		ADD CONSTRAINT uc_Dependent_Id_UserId UNIQUE (Id, UserId)
	END

	ALTER TABLE [dbo].[Dependent] WITH CHECK ADD CONSTRAINT [FK_Dependent_User] FOREIGN KEY([UserId])
	REFERENCES [dbo].[User] ([Id])
END
go
